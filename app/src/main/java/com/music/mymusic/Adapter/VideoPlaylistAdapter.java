package com.music.mymusic.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.music.mymusic.Model.CVideo;
import com.music.mymusic.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class VideoPlaylistAdapter extends RecyclerView.Adapter<VideoPlaylistAdapter.hoder>{

    Context context;
    ArrayList<CVideo> arrayList;
    ItemClick itemClick;

    public void setItemClick(ItemClick itemClick) {
        this.itemClick = itemClick;
    }

    public VideoPlaylistAdapter(Context context, ArrayList<CVideo> arrayList){
        this.context = context;
        this.arrayList = arrayList;
    }

    public int indexSelected = 0;
    @NonNull
    @Override
    public hoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_video2, parent, false);
        return new hoder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull hoder holder, int position) {
        CVideo cVideo = arrayList.get(position);

        holder.txtChu.setText(cVideo.getTitle());
        Picasso.get().load(cVideo.getImage()).into(holder.imgHinh);
        if (position == indexSelected){
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.gray));
        }else {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class hoder extends RecyclerView.ViewHolder{
        ImageView imgHinh;
        TextView txtChu;
        CardView cardView;

        public hoder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardView);
            imgHinh = itemView.findViewById(R.id.image);

            txtChu = itemView.findViewById(R.id.txtChu);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClick.onItemClick(getAdapterPosition());
                }
            });
        }
    }
}
