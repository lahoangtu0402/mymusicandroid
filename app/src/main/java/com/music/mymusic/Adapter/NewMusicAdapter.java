package com.music.mymusic.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.music.mymusic.Model.CPlayList;
import com.music.mymusic.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class NewMusicAdapter extends RecyclerView.Adapter<NewMusicAdapter.hoderHome>{
    Context context;
    ArrayList<CPlayList> cHomeArrayList;
    ItemClick itemClick;

    public void setItemClick(ItemClick itemClick) {
        this.itemClick = itemClick;
    }

    boolean isPlaylistNew;

    public NewMusicAdapter(Context context, ArrayList<CPlayList> cHomeArrayList, boolean isPlaylistNew) {
        this.context = context;
        this.cHomeArrayList = cHomeArrayList;

        this.isPlaylistNew = isPlaylistNew;
    }

    @NonNull
    @Override
    public hoderHome onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (isPlaylistNew == true) {
            View view = LayoutInflater.from(context).inflate(R.layout.row_new_music, parent, false);
            return new hoderHome(view);
        }else {
            View view = LayoutInflater.from(context).inflate(R.layout.row_home_body, parent, false);
            return new hoderHome(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull hoderHome holder, int position) {
        CPlayList cHome = cHomeArrayList.get(position);

        holder.txt_chu.setText(cHome.getTen());
        holder.txtChitiet.setText(cHome.getChitiet());
        Picasso.get().load(cHome.getHinh()).into(holder.img_hinh);

    }

    @Override
    public int getItemCount() {
        return cHomeArrayList.size();
    }

    class hoderHome extends RecyclerView.ViewHolder{
        ImageView img_hinh;
        TextView txt_chu,txtChitiet;

        public hoderHome(@NonNull View itemView) {
            super(itemView);
            img_hinh = itemView.findViewById(R.id.imgHinh);
            txt_chu = itemView.findViewById(R.id.txtChu);
            txtChitiet = itemView.findViewById(R.id.txtChitiet);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClick.onItemClick(getAdapterPosition());
                }
            });
        }
    }
}
