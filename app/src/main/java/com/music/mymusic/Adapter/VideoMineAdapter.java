package com.music.mymusic.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.music.mymusic.Model.CPlayList;
import com.music.mymusic.Model.CVideo;
import com.music.mymusic.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class VideoMineAdapter extends RecyclerView.Adapter<VideoMineAdapter.hoderHome>{
    Context context;
    ArrayList<CVideo> cHomeArrayList;
    ItemClick itemClick;

    public void setItemClick(ItemClick itemClick) {
        this.itemClick = itemClick;
    }

    public VideoMineAdapter(Context context, ArrayList<CVideo> cHomeArrayList) {
        this.context = context;
        this.cHomeArrayList = cHomeArrayList;
    }

    @NonNull
    @Override
    public hoderHome onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_mine, parent, false);
        return new hoderHome(view);
    }

    @Override
    public void onBindViewHolder(@NonNull hoderHome holder, int position) {
        CVideo cHome = cHomeArrayList.get(position);

        holder.txt_chu.setText(cHome.getTitle());
        Picasso.get().load(cHome.getImage()).into(holder.img_hinh);

    }

    @Override
    public int getItemCount() {
        return cHomeArrayList.size();
    }

    class hoderHome extends RecyclerView.ViewHolder{
        ImageView img_hinh;
        TextView txt_chu;

        public hoderHome(@NonNull View itemView) {
            super(itemView);
            img_hinh = itemView.findViewById(R.id.imgHinh);
            txt_chu = itemView.findViewById(R.id.txtChu);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClick.onItemClick(getAdapterPosition());
                }
            });
        }
    }
}
