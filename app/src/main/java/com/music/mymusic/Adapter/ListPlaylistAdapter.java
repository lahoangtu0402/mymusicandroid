package com.music.mymusic.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.music.mymusic.Model.CPlayList;
import com.music.mymusic.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

public class ListPlaylistAdapter extends RecyclerView.Adapter<ListPlaylistAdapter.hoder>{

    Context context;
    ArrayList<CPlayList> cPlayLists;
    ItemClick itemClick;

    public void setItemClick(ItemClick itemClick) {
        this.itemClick = itemClick;
    }

    public ListPlaylistAdapter(Context context, ArrayList<CPlayList> cPlayLists) {

        this.context = context;
        this.cPlayLists = cPlayLists;

    }

    @NonNull
    @Override
    public hoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.raw_playlist, parent, false);
        return new hoder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull hoder holder, int position) {

        CPlayList cMoodGenres = cPlayLists.get(position);
        holder.txtChu.setText(cMoodGenres.getTen());
        Picasso.get().load(cMoodGenres.getHinh()).into(holder.imageView);
//
//        holder.lnTemp.setBackgroundColor(getRandomColor());
    }
    public int getRandomColor(){
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    @Override
    public int getItemCount() {
        return cPlayLists.size();
    }

    class hoder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView txtChu;

        public hoder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imgHinh);
            txtChu = itemView.findViewById(R.id.txtChu);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClick.onItemClick(getAdapterPosition());
                }
            });
        }
    }
}
