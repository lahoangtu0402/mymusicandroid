package com.music.mymusic.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.music.mymusic.R;

import java.util.ArrayList;
import java.util.Random;

public class AdapterMoodGenres extends RecyclerView.Adapter<AdapterMoodGenres.hoder>{

    Context context;
    ArrayList<CCategory> cMoodGenresArrayList;
    ItemClick itemClick;

    public void setItemClick(ItemClick itemClick) {
        this.itemClick = itemClick;
    }

    public AdapterMoodGenres(Context context, ArrayList<CCategory> cMoodGenresArrayList) {

        this.context = context;
        this.cMoodGenresArrayList = cMoodGenresArrayList;

    }

    @NonNull
    @Override
    public hoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_mood_genres, parent, false);
        return new hoder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull hoder holder, int position) {

        CCategory cMoodGenres = cMoodGenresArrayList.get(position);
        holder.txtChu.setText(cMoodGenres.getName());

        holder.lnTemp.setBackgroundColor(getRandomColor());
    }
    public int getRandomColor(){
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    @Override
    public int getItemCount() {
        return cMoodGenresArrayList.size();
    }

    class hoder extends RecyclerView.ViewHolder{

        LinearLayout lnTemp;
        TextView txtChu;

        public hoder(@NonNull View itemView) {
            super(itemView);

            lnTemp = itemView.findViewById(R.id.lnTemp);
            txtChu = itemView.findViewById(R.id.txtChu);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClick.onItemClick(getAdapterPosition());
                }
            });
        }
    }
}
