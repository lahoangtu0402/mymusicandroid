package com.music.mymusic.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.music.mymusic.Model.CVideo;
import com.music.mymusic.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.hoder>{

    Context context;
    ArrayList<CVideo> arrayList;
    ItemClick itemClick;

    public void setItemClick(ItemClick itemClick) {
        this.itemClick = itemClick;
    }

    boolean isGrid = false;
    public VideoAdapter(Context context, ArrayList<CVideo> arrayList,boolean isGrid){
        this.context = context;
        this.arrayList = arrayList;
        this.isGrid = isGrid;
    }
    public VideoAdapter(Context context, ArrayList<CVideo> arrayList){
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public hoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (isGrid == true){
            View view = LayoutInflater.from(context).inflate(R.layout.row_video, parent, false);
            return new hoder(view);
        }else {
            View view = LayoutInflater.from(context).inflate(R.layout.row_video2, parent, false);
            return new hoder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull hoder holder, int position) {
        CVideo cVideo = arrayList.get(position);

        holder.txtChu.setText(cVideo.getTitle());
        Picasso.get().load(cVideo.getImage()).into(holder.imgHinh);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class hoder extends RecyclerView.ViewHolder{

        ImageView imgHinh;
        TextView txtChu;

        public hoder(@NonNull View itemView) {
            super(itemView);

            imgHinh = itemView.findViewById(R.id.image);

            txtChu = itemView.findViewById(R.id.txtChu);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClick.onItemClick(getAdapterPosition());
                }
            });
        }
    }
}
