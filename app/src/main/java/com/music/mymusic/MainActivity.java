package com.music.mymusic;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.mp4.Mp4Extractor;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;
import com.music.mymusic.Database.CGroupHelper;
import com.music.mymusic.Database.CVideoHelper;
import com.music.mymusic.Database.DownloadHelper;
import com.music.mymusic.Fragment.DetailPlaylistFragment;
import com.music.mymusic.Fragment.HomeFragment;
import com.music.mymusic.Fragment.MineFragment;
import com.music.mymusic.Fragment.PlaylistFragment;
import com.music.mymusic.Fragment.SearchFragment;
import com.music.mymusic.Model.AsyntaskDownload;
import com.music.mymusic.Model.AsyntaskDownloadCallback;
import com.music.mymusic.Model.CGroup;
import com.music.mymusic.Model.CVideo;
import com.music.mymusic.Model.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import at.huber.youtubeExtractor.VideoMeta;
import at.huber.youtubeExtractor.YouTubeExtractor;
import at.huber.youtubeExtractor.YtFile;

public class MainActivity extends AppCompatActivity implements AsyntaskDownloadCallback {
    public static String API_key = "AIzaSyDj3ofX4rAx76SuGdYrLxG_uE9NvMLU_pI";

    //Instance
    private static MainActivity activity;
    public static MainActivity getInstance(){
        if(activity == null){
            activity = new MainActivity();
        }
        return activity;
    }
    public static Context getContext() {
        return activity.getApplicationContext();
    }
    public FragmentManager fragmentManager = getSupportFragmentManager();

    public BottomNavigationView navigation;

    public SlideUp slideUp;
    ImageView image_closeSliup;
    //exo player
    public SimpleExoPlayer player;
    private MediaSource mediaSource;
    MediaSource audio;
    boolean checkFavourite = false;
    public int checkRepeat;
    DataSource.Factory dataSourceFactory;
    DefaultExtractorsFactory extractorsFactory;
    public TextView slupName;
    public TextView slupSinger;
    SpinKitView spin_load_play;


    PlayerControlView controls;
    TextView nameSong,nameArtis,nameAlbum;
    ImageButton imagePrev,imageNext,exo_play,exo_pause,exo_shuffle,btn_repeat;
    //video view
    public PlayerView player_view;
    ImageView imageFullscreen;

    //

    ImageView imageClassica;

    ImageView image_playlist,image_fav,image_down;
    CGroupHelper cGroupHelper;

    static int FAVORITES = 0;
    static int WATCHED = 1;
    static int DOWNLOADS = 2;
    DownloadHelper downloadHelper ;
    public FrameLayout fragment_playlist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragment_playlist = findViewById(R.id.fragment_playlist);
        fragment_playlist.setVisibility(View.GONE);
        cGroupHelper = new CGroupHelper(MainActivity.this);

        cGroupHelper.addGroup(new CGroup("Favorites",""));
        cGroupHelper.addGroup(new CGroup("Watched",""));
        cGroupHelper.addGroup(new CGroup("Downloads",""));

        downloadHelper = new DownloadHelper(MainActivity.this);

        activity = this;
        utils = Utils.init();
        navigation = findViewById(R.id.nav_view);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return loadFragment(item.getItemId());
            }
        });
        loadFragment(R.id.navigation_home);
        setSlideUp();
        mapping();
        mappingEXOplayer();
        mappingVideoPlayer();
//        initFullscreenDialog();
        checkRepeat = 1;
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector();
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        playlistCurrent = new ArrayList<>();

        imageClassica = findViewById(R.id.imageClassica);
        imageClassica.setVisibility(View.INVISIBLE);
        imageClassica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaTitle.length() != 0){
                    slideUp.show();
                }

            }
        });
        cVideoHelper = new CVideoHelper(MainActivity.this);
        image_playlist = findViewById(R.id.image_playlist);
        image_playlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment_playlist.setVisibility(View.VISIBLE);
                PlaylistFragment fragment = new PlaylistFragment();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_bottom,  R.anim.slide_in_top,R.anim.fade_in,R.anim.slide_out_bottom);
                transaction.replace(R.id.fragment_playlist, fragment);
                Bundle bundle = new Bundle();
                Gson gson = new Gson();
                String data = gson .toJson(playlistCurrent);;
                bundle.putString("data", data);
                bundle.putInt("position",position);
//                bundle.putString("tittle", cPlayList.getTen()+"");
//                bundle.putString("image", cPlayList.getHinh()+"");
                fragment.setArguments(bundle);

                transaction.addToBackStack(null);
                transaction.commit();
                Animation slideUp = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
                if (navigation.getVisibility() == View.VISIBLE) {
                    navigation.startAnimation(slideUp);
                    //    mainActivity.navigation.setAlpha(0);
                    navigation.setVisibility(View.GONE);
                }
            }
        });
        image_fav = findViewById(R.id.image_fav);
        image_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFavourite == false){
                    cVideoHelper.addVideo(FAVORITES,currSong);
                    isFavourite = true;
                    image_fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_favorite_24));
                   // currSong
                }else {
                    cVideoHelper.deleteVideo(FAVORITES,currSong.getId());
                    isFavourite = false;
                    image_fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_favorite_border_24));
                }
                try {
                    MineFragment mineFragment = MineFragment.getInstance();
                    mineFragment.loadFav();
                }catch (Exception e){

                }
            }
        });
        image_down = findViewById(R.id.image_down);
        image_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fileName = currSong.getTitle()+".mp4";
                String storagePath = Environment.getExternalStorageDirectory()+ File.separator + "MyMusic";
                Log.d("storagePath", " storagePathClick: "+ storagePath + "");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                        && ContextCompat.checkSelfPermission(getContext(),
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(getContext(),
                        android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
                } else {
                    //check download
                    //Download

                    if (downloadHelper.checkExist(currSong) == false){
                        Toast.makeText(MainActivity.this, "Downloading "+currSong.getTitle(), Toast.LENGTH_SHORT).show();
                        downloadHelper.addVideo(currSong);
                        AsyntaskDownload asyntaskDownloadE = new AsyntaskDownload(MainActivity.this,currSong.getTitle());
                        asyntaskDownloadE.response = MainActivity.this;
                        asyntaskDownloadE.execute(currSong.getId());
                    }
                   else {
                        Toast.makeText(MainActivity.this, "Can't download, file is exist", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
    }

    @Override
    public void onErrorAsyntaskDownload(String message) {

    }

    @Override
    public void onResponseAsyntaskDownload(String key,String title) {
        Log.d("onResponse",key);
        Toast.makeText(MainActivity.this, title + "Download done !", Toast.LENGTH_SHORT).show();
        DownloadHelper downloadHelper = new DownloadHelper(MainActivity.this);

        long temp = downloadHelper.updateGroupName(key,1);
        Log.d("update",key + "_"+temp);
        try {
            MineFragment mineFragment = MineFragment.getInstance();
            mineFragment.loadDownload();
        }catch (Exception e){

        }

    }


    CVideoHelper cVideoHelper;
    boolean isFavourite = false;

    void mappingVideoPlayer(){
        player_view  = findViewById(R.id.player_view);
        player_view.setShutterBackgroundColor(Color.TRANSPARENT);
        player_view.setKeepContentOnPlayerReset(true);
//        main_media_frame = findViewById(R.id.main_media_frame);

    }
    private void mappingEXOplayer(){
        btn_repeat = findViewById(R.id.btn_repeat);
        controls = findViewById(R.id.controls);
        imageNext = findViewById(R.id.btn_next);
        imagePrev = findViewById(R.id.btn_prev);
        exo_play = findViewById(R.id.exo_play);
        exo_pause = findViewById(R.id.exo_pause);
        exo_shuffle = findViewById(R.id.btn_shuffle);
        exo_shuffle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickShuffle();
            }
        });
        imageNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNextAudio(true);
            }
        });
        imagePrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               setNextAudio(false);
            }
        });

        btn_repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickRepeat();
            }
        });

        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector();
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        clickRepeat();

//       // imageFullscreen = findViewById(R.id.imageFullscreen);
//        imageFullscreen.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
//              //  openFullscreenDialog();
//            }
//        });

    }
    void setSlideUp(){
        View slideView = findViewById(R.id.slideView);
        slideUp = new SlideUpBuilder(slideView)
                .withStartState(SlideUp.State.HIDDEN)
                .withStartGravity(Gravity.BOTTOM)
                .build();
        slideUp.addSlideListener(new SlideUp.Listener.Visibility() {
            @Override
            public void onVisibilityChanged(int visibility) {
                if (visibility == View.VISIBLE){

                    Animation slideUp = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
                    if (navigation.getVisibility() == View.VISIBLE) {
                        navigation.startAnimation(slideUp);

                       navigation.setVisibility(View.GONE);
                    }
                }else {

                    Animation slideUp = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
                    if (navigation.getVisibility() == View.GONE) {
                        navigation.startAnimation(slideUp);
                        navigation.setAlpha(1);
                        navigation.setVisibility(View.VISIBLE);
                    }
                }

            }
        });
    }
    void mapping(){
        image_closeSliup = findViewById(R.id.image_closeSliup);
        image_closeSliup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideUp.hide();
            }
        });
        nameSong = findViewById(R.id.txt_nameSong);
        nameArtis = findViewById(R.id.txt_nameArtists);
        nameAlbum = findViewById(R.id.txt_albumName);

        //Video
        btn_repeat = findViewById(R.id.btn_repeat);


        controls = findViewById(R.id.controls);
        imageNext = findViewById(R.id.btn_next);
        imagePrev = findViewById(R.id.btn_prev);

        spin_load_play = findViewById(R.id.spin_load_play);

    }
    int position = 0;

    public void initPlayer(){
        if(player!=null){
            //player.stop();
            player.release();
        }

        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        player_view.requestFocus();
        player_view.setPlayer(player);
        player_view.setUseController(false);
        controls.setPlayer(player);
        dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "com.app.musicplayer"), bandwidthMeter);
        extractorsFactory = new DefaultExtractorsFactory();
        extractorsFactory.setMp4ExtractorFlags(Mp4Extractor.FLAG_WORKAROUND_IGNORE_EDIT_LISTS );

    }
    public void playMusicUrl(String urlVideo,String audioUrl){
        //ADD HISTORY
        cVideoHelper.addVideo(WATCHED,currSong);
        initPlayer();
        mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).setExtractorsFactory
                (extractorsFactory).createMediaSource(Uri.parse(urlVideo));
        audio = new ExtractorMediaSource.Factory(dataSourceFactory).setExtractorsFactory
                (extractorsFactory).createMediaSource(Uri.parse(audioUrl));

        if(urlVideo.equals(audioUrl)){
            //mediaSource = new .
            MediaSource dashMediaSource = new DashMediaSource(Uri.parse(urlVideo), dataSourceFactory,
                    new DefaultDashChunkSource.Factory(dataSourceFactory), null, null);
            player.prepare(mediaSource,false,false);
        }else{
            MergingMediaSource mergedSource =
                    new MergingMediaSource(mediaSource, audio);

            player.prepare(mergedSource);
        }


        //player.prepare(mediaSource);
        player.setPlayWhenReady(true);

        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {
//                isCheckLoading = isLoading;
            }

            @SuppressLint("ResourceType")
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                MediaPlayerService mediaPlayerService = MediaPlayerService.getInstance();
//                FragmentPlayBackDisc fragmentPlayback = FragmentPlayBackDisc.getInstance();
//                Start Video
                if (playWhenReady && playbackState == Player.STATE_READY) {



                    playerLoaded();
                    utils.buildNotification( utils.generateAction( android.R.drawable.ic_media_pause, "Pause", mediaPlayerService.ACTION_PAUSE,MainActivity.this ),MainActivity.this );
                    imageClassica.setVisibility(View.VISIBLE);
                } else if (playWhenReady) {
//                    Video End
                    if (playbackState == Player.STATE_ENDED){

                        if(checkRepeat==2)//Stop when end playlists
                        {
                            player.seekTo(0);
                            player.setPlayWhenReady(false);
                            exo_play.setVisibility(View.VISIBLE);
                            exo_pause.setVisibility(View.INVISIBLE);

                        }
                        //if(checkRepeat == 0){
                        else{//checkRepeat == 0 || == 1
                            setNextAudio(true);
                        }

                    }
//                  Player Loading
                    else{
                        playerLoading();
                    }
                }
                //Pause Video
                else {
                    exo_play.setVisibility(View.VISIBLE);
                    exo_pause.setVisibility(View.INVISIBLE);
                    setType(exo_play,true);
                    setType(exo_pause,true);
                    utils.buildNotification(utils.generateAction(android.R.drawable.ic_media_play, "Play", mediaPlayerService.ACTION_PLAY,MainActivity.this),MainActivity.this);
                  //buildNotification(generateAction(android.R.drawable.ic_media_play, "Pause", mediaPlayerService.ACTION_PLAY));

                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {
                if(repeatMode == RESULT_OK){

                }
            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
//                if(rotateAnimation != null){
//                    rotateAnimation.pause();
//                }
                //playWithUrl(,true,arrString);
            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });
    }

    public Utils utils;
    ArrayList<CVideo> playlistCurrent;
    CVideo currSong;
    public boolean isPlayShuffle = false;
    public String mediaLargeIcon ="",mediaTitle = "",mediaArtist = "";
    public void playWithUrl(CVideo song, ArrayList<CVideo> arrString,String albumName, int position){
        albumCurrent = albumName;
        currSong = song;
        isFavourite = cVideoHelper.checkExist(FAVORITES,currSong);
        if (isFavourite == true ){
            image_fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_favorite_24));
        }else {
            image_fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_favorite_border_24));
        }



        this.position = position;
        playlistCurrent.clear();
        playlistCurrent.addAll(arrString);

        mediaLargeIcon = song.getImage();
        mediaTitle = song.getTitle();
        mediaArtist = albumName;

        if(player != null){
            player.release();
        }

        String key,image,title,artist;
        key = song.getId();
        image = song.getImage();
        title = song.getTitle();


        nameAlbum.setText("");
        nameSong.setText(title);
        spin_load_play.setVisibility(View.VISIBLE);
        nameSong.setSelected(true);
        nameArtis.setText(albumName);
        Log.d("keyPlay",key);

        boolean isDownloaded = downloadHelper.checkExist(currSong);
        image_down.setVisibility(View.GONE);
        if (isDownloaded == true){
            image_down.setVisibility(View.GONE);
            String PATH_SUBTITLE = getExternalFilesDir(null).getAbsolutePath() + File.separator + "MyMusicApp"+"/"+key+".mp4";
            String urlVideo = PATH_SUBTITLE;
            playMusicUrl(urlVideo, urlVideo);

        }else{

            new YouTubeExtractor(MainActivity.this) {
                @SuppressLint("CheckResult")
                @Override
                protected void onExtractionComplete(SparseArray<YtFile> ytFiles, VideoMeta videoMeta) {
                    String urlVideo = "";
                    String audio = "";
                    if (ytFiles != null) {
                        int itag = 137;
                        try {
                            urlVideo = ytFiles.get(itag).getUrl();
                        } catch (Exception ex) {
                            itag = 136;
                            try {
                                urlVideo = ytFiles.get(itag).getUrl();
                            } catch (Exception ex1) {
                                itag = 134;
                                try {
                                    urlVideo = ytFiles.get(itag).getUrl();
                                } catch (Exception ex2) {
                                }
                            }
                        }
                        int itagaudio = 140;
                        try {
                            audio = ytFiles.get(itagaudio).getUrl();
                        } catch (Exception ex3) {
                            itagaudio = 139;
                            audio = ytFiles.get(itagaudio).getUrl();
                        }

                        image_down.setVisibility(View.VISIBLE);
                        playMusicUrl(urlVideo, audio);
//                    utils.showNotification(getContext(), title, artist, image);

                    } else {

                        Toast.makeText(MainActivity.this, "This audio can't play, please try another audio", Toast.LENGTH_SHORT).show();
                    }
                }
            }.extract(key, true, false);
        }

        //set Lyric


    }
    public void setNextAudio(boolean check){
        int nextPosition = position;
        player.stop();

        //check : true -> next
        //check : false -> back
        if(check){
            if((getCurrentPosition() != countAllListAudio() -1))
            {
                if(checkRepeat==1)
                {
                    nextPosition = position;

                }

                else {
                    position = getCurrentPosition()+1;
                    nextPosition = position;
                    if(nextPosition > countAllListAudio()-1){
                        nextPosition = 0;
                    }
                }
            }
            else if(isAutoPlay){

                position = getCurrentPosition()+1;
                nextPosition = position;
                if(nextPosition > countAllListAudio()-1){
                    nextPosition = 0;
                }

            }
            else
            {
                position = 0;
                nextPosition = position;
            }
        }
        else{
            if(position != 0){
                position = position - 1;
                nextPosition = position;
            }else{
                position = countAllListAudio()-1;
                nextPosition = position;
            }
        }
        CVideo popular = (CVideo)playlistCurrent.get(nextPosition);
        if(isPlayShuffle && listTempAudio.size()>0){
            popular = (CVideo) listTempAudio.get(nextPosition);
        }
        ArrayList<CVideo> arrayList = new ArrayList<>();
        arrayList.addAll(playlistCurrent);
        playWithUrl(popular, arrayList, albumCurrent,nextPosition);
    }
    ArrayList<CVideo> listTempAudio;
    public void clickShuffle(){
        if(isPlayShuffle){
            isPlayShuffle = false;
            unShuffle();
        }else{
            isPlayShuffle = true;
            shuffleList();
        }
    }
    public void shuffleList(){
        if(playlistCurrent.size() > 0){
            listTempAudio = new ArrayList<>();
            listTempAudio.addAll(playlistCurrent);
        }
        Collections.shuffle(listTempAudio);

        exo_shuffle.setAlpha(isPlayShuffle?1:(float) 0.3);
    }

    public void unShuffle(){
        exo_shuffle.setAlpha(isPlayShuffle?1:(float) 0.3);
    }
    String albumCurrent = "";
    public int getCurrentPosition(){
        int currentPosition = position;
        if(isPlayShuffle){
            for (int i = 0; i < listTempAudio.size(); i++) {
                if (currSong != null && currSong.getId() == ((CVideo)listTempAudio.get(i)).getId()) {
                    currentPosition = i;
                    break;
                }
            }
        }else {
            for (int i = 0; i < playlistCurrent.size(); i++) {
                if (currSong != null && currSong.getId() == ((CVideo)playlistCurrent.get(i)).getId()) {
                    currentPosition = i;
                    break;
                }
            }
        }
        return currentPosition;
    }
    public int countAllListAudio(){
        int count = 0;
        if(isPlayShuffle){
            count = listTempAudio.size();
        }else {
            count = playlistCurrent.size();
        }
        return count;
    }
    public void clickRepeat(){
        //0 repeate all
//        1 repeat 1
//        2 no repeat
        if(checkRepeat == 0){
            player.setRepeatMode(Player.REPEAT_MODE_ONE);
            btn_repeat.setAlpha(1f);
            checkRepeat = 1;
            btn_repeat.setImageDrawable(getResources().getDrawable(R.drawable.icons_repeat_one));
        }
        else if(checkRepeat == 1){
            player.setRepeatMode(Player.REPEAT_MODE_OFF);
            btn_repeat.setImageDrawable(getResources().getDrawable(R.drawable.ic_repeat3));

            btn_repeat.setAlpha(1f);
            checkRepeat = 2;
        }else{
            player.setRepeatMode(Player.REPEAT_MODE_OFF);
            btn_repeat.setImageDrawable(getResources().getDrawable(R.drawable.icons_repeat));
            btn_repeat.setAlpha(1f);
            checkRepeat = 0;
        }

    }
    public boolean isAutoPlay = false;
    boolean isCheckPlaying = false;
    void playerLoading(){
        isCheckPlaying = false;
        spin_load_play.setVisibility(View.VISIBLE);
        setTypeButton(exo_pause,false);
        setTypeButton(exo_play,false);
//        if (mExoPlayerFullscreen && title_video_full != null) {
//            setTypeButton(btn_pause_full,false);
//            setTypeButton(btn_play_full,false);
//            spin_load_play_full.setVisibility(View.VISIBLE);
//        }
    }
    void playerLoaded(){
//
        isCheckPlaying = true;
      spin_load_play.setVisibility(View.INVISIBLE);
        setTypeButton(exo_play,true);
        setTypeButton(exo_pause,true);
//        if (mExoPlayerFullscreen && title_video_full != null) {
//            spin_load_play_full.setVisibility(View.INVISIBLE);
//            setTypeButton(btn_pause_full,true);
//            setTypeButton(btn_play_full,true);
//        }
    }

    void setType(View view,boolean check){
        check = true;
        view.setEnabled(check);
        view.setAlpha(check?1:(float) 0.3);
    }

    void setTypeButton(View view,boolean check){
        view.setEnabled(check);
        view.setAlpha(check?1:(float) 0.3);
    }



    public boolean loadFragment(int id) {
        Fragment fragment = null;
        String backStateName = "home";
        switch (id) {
            case R.id.navigation_home:
                fragment = HomeFragment.getInstance();
                break;

            case R.id.navigation_search:
                fragment = SearchFragment.getInstance();
                backStateName = "search";
                break;
            case R.id.navigation_mine:
                fragment = new MineFragment();
                backStateName = "mine";
                break;

        }

        replaceFragment(fragment,backStateName,id);
        return true;
    }
    public void replaceFragment (Fragment fragment, String backStateName, int i){
        FragmentManager fragmentManager = getSupportFragmentManager();
        if(fragmentManager.findFragmentByTag(backStateName) == null){
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.fragment_container, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
        }else{
            getSupportFragmentManager().popBackStack(backStateName,i);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(1);
        Intent intent = new Intent( getApplicationContext(), MediaPlayerService.class );
        stopService( intent );
        if(player != null)
            player.release();
    }
    public static MediaPlayerService mediaPlayerService;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void buildNotification(Notification.Action action) {

        mediaPlayerService = MediaPlayerService.getInstance();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.MediaStyle style = new Notification.MediaStyle();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("DATA", "notification");
        Bitmap bitmap_image = BitmapFactory.decodeResource(getResources(), R.drawable.iconapp);
        PendingIntent pendingIntent1 = PendingIntent.getService(getApplicationContext(), 1, intent, 0);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
////        int notifyID = 1;
        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        //CharSequence name = getString(R.string.channel_name);// The user-visible name of the channel.
        Notification.Builder builder = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "chickentheusa", NotificationManager.IMPORTANCE_DEFAULT);
            //setting pattern to disable vibrating
            notificationChannel.setVibrationPattern(new long[]{0});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
            builder = new Notification.Builder(this, CHANNEL_ID);
        } else {
            builder = new Notification.Builder(this);
        }
        builder.setSmallIcon(R.drawable.ic_launcher_background);
        builder.setContentTitle(mediaTitle);
        builder.setContentText(mediaArtist);
        builder.setLargeIcon(bitmap_image);
        builder.setContentIntent(pendingIntent);
        builder.setDeleteIntent(pendingIntent1);
        builder.setOngoing(true);
        builder.build().flags = Notification.FLAG_ONGOING_EVENT;
        builder.setStyle(style);
        builder.addAction(generateAction(android.R.drawable.ic_media_previous, "Previous", mediaPlayerService.ACTION_PREVIOUS));
        builder.addAction(action);
        builder.addAction(generateAction(android.R.drawable.ic_media_next, "Next", mediaPlayerService.ACTION_NEXT));
        builder.addAction(generateAction(android.R.drawable.ic_menu_close_clear_cancel, "Stop", mediaPlayerService.ACTION_STOP));
        style.setShowActionsInCompactView(0, 1, 2, 3, 4);
        notificationManager.notify(1, builder.build());
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT_WATCH)
    public Notification.Action generateAction(int icon, String title, String intentAction) {
        Intent intent = new Intent(getApplicationContext(), MediaPlayerService.class);
        intent.setAction(intentAction);
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1, intent, 0);
        return new Notification.Action.Builder(icon, title, pendingIntent).build();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(slideUp.isVisible()){
            slideUp.hide();
        }
    }


}