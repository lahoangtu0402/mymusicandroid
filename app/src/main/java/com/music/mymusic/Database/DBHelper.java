package com.music.mymusic.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "music.db";
    private static final int DATABASE_VERSION = 1;
    private static Context c;

    //single stance
    private static DBHelper instance;
    public static DBHelper getInstance(Context context) {
        if (instance == null) {
            synchronized (DBHelper.class) {
                instance = new DBHelper(context, "music2.sqlite", null, 1);
            }
        }
        return instance;
    }

//    //New DB
    final String SQL_CREATE_TABLE_GROUP_CONTRACT =
        "CREATE TABLE " + CGroupHelper.TABLE_NAME + " (" +
                CGroupHelper._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                CGroupHelper.NAME_GROUP + "  TEXT NOT NULL UNIQUE,"+
                CGroupHelper.IMAGE + "  TEXT,"+
                CGroupHelper.CREATE_AT + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP " +
                ")";
    final String SQL_CREATE_TABLE_VIDEO =
            "CREATE TABLE " + CVideoHelper.TABLE_NAME + " (" +
                    CVideoHelper._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    CVideoHelper.TITLE + " TEXT NOT NULL ,"+
                    CVideoHelper.IMAGE + " TEXT,"+
                    CVideoHelper.KEY + "  TEXT,"+
                    CVideoHelper.LINK + "  TEXT,"+
                    CVideoHelper.CGROUP + "  INTEGER,"+
                    CVideoHelper.CREATE_AT + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP " +
                    ")";
    final String SQL_CREATE_TABLE_DOWNLOAD =
            "CREATE TABLE " + DownloadHelper.TABLE_NAME + " (" +
                    DownloadHelper._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    DownloadHelper.TITLE + " TEXT NOT NULL ,"+
                    DownloadHelper.IMAGE + " TEXT,"+
                    DownloadHelper.KEY + "  TEXT,"+
                    DownloadHelper.STATUS + "  INTEGER,"+
                    DownloadHelper.CREATE_AT + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP " +
                    ")";



    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void QueryData(String sql)
    {
        try {
            SQLiteDatabase database = getWritableDatabase();
            database.execSQL(sql);
        }catch (Exception e){

        }
    }

    public Cursor GetData(String sql)
    {
        try {
            SQLiteDatabase database = getReadableDatabase();
            return database.rawQuery(sql,null);
        }catch (Exception e){
            return null;
        }
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        //For New User
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_GROUP_CONTRACT);
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_VIDEO);
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_DOWNLOAD);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        //For User Update
        if(newVersion>oldVersion){


        }else {

        }
    }
}
