package com.music.mymusic.Database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import com.music.mymusic.Model.CGroup;
import com.music.mymusic.Model.CVideo;

import java.util.ArrayList;

public class DownloadHelper implements BaseColumns {
    public static final String TABLE_NAME = "download_table";
    public static final String TITLE = "TITLE";
    public static final String IMAGE = "IMAGE";
    public static final String KEY = "KEY_YOUTUBE";
    public static final String STATUS = "STATUS_DOWNLOAD";
    public static final String CREATE_AT = "createat";



    //
    public static DBHelper dbHelper;
    private SQLiteDatabase db;
    public DownloadHelper(Activity activity) {
        dbHelper = new DBHelper(activity);
        db = dbHelper.getWritableDatabase();
    }
    // lay ra all
    public ArrayList<CVideo> getList(){
        //
        ArrayList<CVideo> list = new ArrayList<>();

        // Select All Query
        String sql = "SELECT * FROM "+ DownloadHelper.TABLE_NAME +" ORDER BY " + DownloadHelper.CREATE_AT + " DESC  ;";
        Cursor cursor =  dbHelper.GetData(sql);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String name =  cursor.getString(cursor.getColumnIndex(DownloadHelper.TITLE));
                String image =  cursor.getString(cursor.getColumnIndex(DownloadHelper.IMAGE));
                String key =  cursor.getString(cursor.getColumnIndex(DownloadHelper.KEY));
                int statusDownload =  cursor.getInt(cursor.getColumnIndex(DownloadHelper.STATUS));
                //
                CVideo note = new CVideo(key,name,"",image,statusDownload);
                list.add(note);
            } while (cursor.moveToNext());
        }

        return list;
    }

    //
    public boolean checkExist(CVideo info){
        String sql = "SELECT * FROM "+ DownloadHelper.TABLE_NAME + " WHERE "+ DownloadHelper.KEY+" = '"+info.getId()+"' ORDER BY " + DownloadHelper.CREATE_AT + " DESC  ;";
        Cursor cursor =  dbHelper.GetData(sql);
        if (cursor.getCount() == 0){
            return  false;
        }
        return  true;
    }

    // them
    public boolean addVideo(CVideo info){

        ContentValues contentValues = new ContentValues();
        contentValues.put(DownloadHelper.TITLE,info.getTitle());
        contentValues.put(DownloadHelper.IMAGE,info.getImage());
        contentValues.put(DownloadHelper.KEY,info.getId());
        contentValues.put(DownloadHelper.STATUS,0);

        long result = 0;
        try {
            db = dbHelper.getWritableDatabase();
            result = db.insert(DownloadHelper.TABLE_NAME, null, contentValues);
        }catch(Exception ex) {
            Log.e("Error DB", ex.getMessage());
        }
        return result > 0;
    }

    // xoa
    public boolean deleteVideo(String key){
        int i = db.delete(DownloadHelper.TABLE_NAME,//ten bang
                DownloadHelper.KEY+" = ?",//dieu kien
                new String[] { key });
        if(i>0){return true;}
        return false;
    }

//
//    //count
//    public int getCountGroup(){
//        String sql = "SELECT * FROM "+ CGroupHelper.TABLE_NAME + " ORDER BY " + CGroupHelper.CREATE_AT + " DESC  ;";
//        Cursor cursor =  dbHelper.GetData(sql);
//        return cursor.getCount();
//    }
    public long updateGroupName(String key,int status){

        //"UPDATE Student SET name = ? and image=? Where id = ?"
        ContentValues contentValues = new ContentValues();
        contentValues.put(DownloadHelper.STATUS, status);//ten cot, gia tri cot

        long result = db.update(DownloadHelper.TABLE_NAME,
                contentValues,
                DownloadHelper.KEY+" = ?",
                new String[] { key });

        return result;

    }

}
