package com.music.mymusic.Database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import com.music.mymusic.Model.CGroup;

import java.util.ArrayList;

public class CGroupHelper implements BaseColumns {
    public static final String TABLE_NAME = "group_music";
    public static final String NAME_GROUP = "name_group";
    public static final String IMAGE = "image_group";
    public static final String CREATE_AT = "createat";

    //


    public static DBHelper dbHelper;
    private SQLiteDatabase db;
    public CGroupHelper(Activity activity) {
        dbHelper = new DBHelper(activity);
        db = dbHelper.getWritableDatabase();
    }

    // lay ra all
    public ArrayList<CGroup> getListGroup(){
        //
        ArrayList<CGroup> list = new ArrayList<CGroup>();

        // Select All Query
        String sql = "SELECT * FROM "+ CGroupHelper.TABLE_NAME + " ORDER BY " + CGroupHelper.CREATE_AT + " DESC  ;";

        Cursor cursor =  dbHelper.GetData(sql);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(0);
                String name = cursor.getString(1); // String name2 = cursor.getString(cursor.getColumnIndex(CGroupContractHelper.NAME_GROUP));
                String image = cursor.getString(2);
                //
                CGroup note = new CGroup(id,name,image);
                list.add(note);
            } while (cursor.moveToNext());
        }

        return list;
    }


    //lay ra theo id

    public CGroup getGroupByID(int id){
        String sql = "SELECT * FROM "+ CGroupHelper.TABLE_NAME + " WHERE "+ CGroupHelper._ID + " = "+id;
        Cursor cursor =  dbHelper.GetData(sql);
        if (cursor != null){
            cursor.moveToFirst();
        }
        int idnew = cursor.getInt(0);
        String name = cursor.getString(1); // String name2 = cursor.getString(cursor.getColumnIndex(CGroupContractHelper.NAME_GROUP));
        String image = cursor.getString(2);
        //
        CGroup note = new CGroup(idnew,name,image);
        return note;
    }

    // them
    public boolean addGroup(CGroup info){
        ContentValues contentValues = new ContentValues();
        contentValues.put(CGroupHelper.NAME_GROUP,info.getName());
        contentValues.put(CGroupHelper.IMAGE,info.getImage());

        long result = 0;
        try {
            db = dbHelper.getWritableDatabase();
            result = db.insert(CGroupHelper.TABLE_NAME, null, contentValues);
        }catch(Exception ex) {
            Log.e("Error DB", ex.getMessage());
        }
        return result > 0;
    }

    // xoa
    public boolean deleteGroup(String name){
        int i = db.delete(CGroupHelper.TABLE_NAME,//ten bang
                CGroupHelper.NAME_GROUP+" = ?",//dieu kien
                new String[] { name });
        if(i>0){return true;}
        return false;
    }

    //sua
    public long updateGroupName(CGroup groupNew){

        //"UPDATE Student SET name = ? and image=? Where id = ?"
        ContentValues contentValues = new ContentValues();
        contentValues.put(CGroupHelper.NAME_GROUP,groupNew.getName() );//ten cot, gia tri cot
//        contentValues.put("firstname",contacts.getFirstname());
//        contentValues.put("lastname",contacts.getLastname());


        long result = db.update(CGroupHelper.TABLE_NAME,
                contentValues,
                CGroupHelper._ID+" = " + groupNew.getId(),
                null);

        return result;

    }


    //count
    public int getCountGroup(){
        String sql = "SELECT * FROM "+ CGroupHelper.TABLE_NAME + " ORDER BY " + CGroupHelper.CREATE_AT + " DESC  ;";
        Cursor cursor =  dbHelper.GetData(sql);
        return cursor.getCount();
    }

}
