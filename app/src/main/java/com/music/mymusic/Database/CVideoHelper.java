package com.music.mymusic.Database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import com.music.mymusic.Model.CGroup;
import com.music.mymusic.Model.CVideo;

import java.util.ArrayList;

public class CVideoHelper implements BaseColumns {
    public static final String TABLE_NAME = "cvideo";
    public static final String TITLE = "TITLE";
    public static final String IMAGE = "IMAGE";
    public static final String KEY = "KEY_YOUTUBE";
    public static final String LINK = "LINKYOUTUBE";
    public static final String CREATE_AT = "createat";
    public static final String CGROUP = "group_video";


    //
    public static DBHelper dbHelper;
    private SQLiteDatabase db;
    public CVideoHelper(Activity activity) {
        dbHelper = new DBHelper(activity);
        db = dbHelper.getWritableDatabase();
    }
    // lay ra all
    public ArrayList<CVideo> getListByGroup(int idGroup){
        //
        ArrayList<CVideo> list = new ArrayList<>();

        // Select All Query
        String sql = "SELECT * FROM "+ CVideoHelper.TABLE_NAME + " WHERE "+CVideoHelper.CGROUP +" = "+idGroup+" ORDER BY " + CVideoHelper.CREATE_AT + " DESC  ;";
        Cursor cursor =  dbHelper.GetData(sql);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String name =  cursor.getString(cursor.getColumnIndex(CVideoHelper.TITLE));
                String image =  cursor.getString(cursor.getColumnIndex(CVideoHelper.IMAGE));
                String key =  cursor.getString(cursor.getColumnIndex(CVideoHelper.KEY));
                String link =  cursor.getString(cursor.getColumnIndex(CVideoHelper.LINK));
                //
                CVideo note = new CVideo(key,name,"",image,link);
                list.add(note);
            } while (cursor.moveToNext());
        }

        return list;
    }

    //
    public boolean checkExist(int idGroup, CVideo info){
        String sql = "SELECT * FROM "+ CVideoHelper.TABLE_NAME + " WHERE "+CVideoHelper.CGROUP +" = "+idGroup+" AND "+CVideoHelper.KEY+" = '"+info.getId()+"' ORDER BY " + CVideoHelper.CREATE_AT + " DESC  ;";
        Cursor cursor =  dbHelper.GetData(sql);
        if (cursor.getCount() == 0){
            return  false;
        }
        return  true;
    }

    // them
    public boolean addVideo(int idGroup, CVideo info){
        if (checkExist(idGroup,info)){
            deleteVideo(idGroup,info.getId());
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(CVideoHelper.TITLE,info.getTitle());
        contentValues.put(CVideoHelper.IMAGE,info.getImage());
        contentValues.put(CVideoHelper.KEY,info.getId());
        contentValues.put(CVideoHelper.LINK,info.getLinkDownload());
        contentValues.put(CVideoHelper.CGROUP,idGroup);
        long result = 0;
        try {
            db = dbHelper.getWritableDatabase();
            result = db.insert(CVideoHelper.TABLE_NAME, null, contentValues);
        }catch(Exception ex) {
            Log.e("Error DB", ex.getMessage());
        }
        return result > 0;
    }

    // xoa
    public boolean deleteVideo(int idGroup, String key){
        int i = db.delete(CVideoHelper.TABLE_NAME,//ten bang
                CVideoHelper.CGROUP+" = "+idGroup+" AND " + CVideoHelper.KEY + " = ?",//dieu kien
                new String[] { key });
        if(i>0){return true;}
        return false;
    }

//
//    //count
//    public int getCountGroup(){
//        String sql = "SELECT * FROM "+ CGroupHelper.TABLE_NAME + " ORDER BY " + CGroupHelper.CREATE_AT + " DESC  ;";
//        Cursor cursor =  dbHelper.GetData(sql);
//        return cursor.getCount();
//    }

}
