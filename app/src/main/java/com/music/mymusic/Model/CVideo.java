package com.music.mymusic.Model;

public class CVideo {
    String id,title,description,image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public CVideo(String id, String title, String description, String image) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.image = image;
    }
    String linkDownload = "";

    public String getLinkDownload() {
        return linkDownload;
    }

    public void setLinkDownload(String linkDownload) {
        this.linkDownload = linkDownload;
    }

    public CVideo(String id, String title, String description, String image, String linkDownload) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.image = image;
        this.linkDownload = linkDownload;
    }

    int statusDownload = 0;

    public CVideo(String id, String title, String description, String image, int statusDownload) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.image = image;
        this.statusDownload = statusDownload;
    }

    public int getStatusDownload() {
        return statusDownload;
    }

    public void setStatusDownload(int statusDownload) {
        this.statusDownload = statusDownload;
    }
}
