package com.music.mymusic.Model;

public interface AsyntaskDownloadCallback {
    void onErrorAsyntaskDownload(String message);
    void onResponseAsyntaskDownload(String key,String link);
}
