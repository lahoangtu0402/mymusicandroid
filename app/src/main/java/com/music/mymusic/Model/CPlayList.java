package com.music.mymusic.Model;

public class CPlayList {
    String hinh, ten, playList_key, chitiet;

    public CPlayList(String hinh, String ten, String playList_key, String chitiet) {
        this.hinh = hinh;
        this.ten = ten;
        this.playList_key = playList_key;
        this.chitiet = chitiet;
    }

    public String getHinh() {
        return hinh;
    }

    public void setHinh(String hinh) {
        this.hinh = hinh;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getPlayList_key() {
        return playList_key;
    }

    public void setPlayList_key(String playList_key) {
        this.playList_key = playList_key;
    }

    public String getChitiet() {
        return chitiet;
    }

    public void setChitiet(String chitiet) {
        this.chitiet = chitiet;
    }
}
