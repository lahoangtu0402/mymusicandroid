/*
 * Copyright (C) 2017 Anupam Das
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.music.mymusic.Model;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.util.SparseArray;


import com.music.mymusic.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import at.huber.youtubeExtractor.VideoMeta;
import at.huber.youtubeExtractor.YouTubeExtractor;
import at.huber.youtubeExtractor.YtFile;

public class AsyntaskDownload extends AsyncTask<String, Integer, Void> {
    private final Context mContext;
    public AsyntaskDownloadCallback response = null;
    String titleVideo = "";
    private int optionUser;
    public AsyntaskDownload(Context context,String title)
    {
        mContext = context;
        titleVideo = title;
    }
    public AsyntaskDownload(Context context, int optionUser)
    {
        mContext = context;
        this.optionUser = optionUser;
    }
    public ArrayList<Object> lstData = new ArrayList<>();

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        Log.d("Download",values + "");
    }

    @Override
    protected Void doInBackground(String... params) {
        try {
            new YouTubeExtractor(mContext) {
                @SuppressLint("CheckResult")
                @Override
                protected void onExtractionComplete(SparseArray<YtFile> ytFiles, VideoMeta videoMeta) {
                    String urlMP4 = "";
                    urlMP4 = ytFiles.get(18).getUrl();
                    saveMP4File(urlMP4,params[0]+"");
                }
            }.extract(params[0], true, true);
        }catch (Exception e){
            Log.e("Err",e.getMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
    }


    public void saveMP4File(String urlMP4, String fileName){
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
//                    String storagePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
//                            + File.separator + "My_Video";

//                    String storagePath = Environment.getExternalStorageDirectory()+ File.separator + "MyMusic";
//                    Log.d("storagePath","saveMP4File: "+storagePath+"");
                    URL u = null;
                    InputStream is = null;
                    try {
                        u = new URL(urlMP4);
                        is = u.openStream();
                        HttpURLConnection huc = (HttpURLConnection) u.openConnection(); //to know the size of video
                        int size = huc.getContentLength();
                        String PATH_SUBTITLE = mContext.getExternalFilesDir(null).getAbsolutePath() + File.separator + "MyMusicApp";
                        Log.d("storagePath","saveMP4File: "+PATH_SUBTITLE+"");
                        if (huc != null) {
//                            //File f = new File(storagePath, fileName+".mp4");
//                            if (!new File(storagePath).exists()) {
//                                new File(storagePath).mkdirs();
//                            }
//                            File storageDir = new File(Environment.getExternalStorageDirectory().toString(), "MyMusic");
//                            storageDir.mkdirs();
//
//                            File f = File.createTempFile(
//                                    fileName,  // prefix
//                                    ".mp4",         // suffix
//                                    storageDir      // directory
//                            );
                            File sourceLocation = new File (PATH_SUBTITLE+"/"+fileName+".mp4");
                            File file = new File("/" + PATH_SUBTITLE + "/"+fileName+".mp4");

                            File f = new File(PATH_SUBTITLE, fileName+".mp4");
                            if (!f.getParentFile().exists())
                                f.getParentFile().mkdirs();
                            if (!f.exists())
                                f.createNewFile();

                            FileOutputStream fos = new FileOutputStream(f);
                            byte[] buffer = new byte[1024];
                            int len1 = 0;
                            if (is != null) {
                                while ((len1 = is.read(buffer)) > 0) {
                                    fos.write(buffer, 0, len1);
                                }
                            }
                            if (fos != null) {
                                fos.close();
                            }
                            ((Activity) mContext).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    response.onResponseAsyntaskDownload(fileName,titleVideo);
                                }
                            });
                        }
                    } catch (Exception mue) {

                        mue.printStackTrace();
                    }finally {
                        try {
                            if (is != null) {
                                is.close();
                            }
                        } catch (IOException ioe) {
                            // just going to ignore this one
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        };
        thread.start();
    }

//    //lấy link download theo option user
//    public String getLinkMP4Download(SparseArray<YtFile> ytFiles, int optionUser)
//    {
//        String urlMP4 = "";
//
//        if(optionUser==1)
//        {
//            if (ytFiles != null) {
//                int itag = 134;//360kps
//                try {
//                    urlMP4 = ytFiles.get(itag).getUrl();
//                } catch (Exception ex) {
//                    itag = 133;//240p
//                    try {
//                        urlMP4 = ytFiles.get(itag).getUrl();
//                    } catch (Exception ex1) {
//                        itag = 17;//144kps
//                        try {
//                            urlMP4 = ytFiles.get(itag).getUrl();
//                        } catch (Exception ex2) {
//                        }
//                    }
//                }
//                Log.d("itagURL","128kps: "+itag);
//            }
//
//        }
//        else//320kps
//        {
//            if (ytFiles != null) {
//                int itag = 136;//720p
//                try {
//                    urlMP4 = ytFiles.get(itag).getUrl();
//                } catch (Exception ex) {
//                    Log.d("itagURL","ex:" +ex.getMessage());
//                    itag = 135;//480p
//                    try {
//                        urlMP4 = ytFiles.get(itag).getUrl();
//                    } catch (Exception ex1) {
//                        Log.d("itagURL","ex1:" +ex.getMessage());
//                        itag = 134;//360kps
//                        try {
//                            urlMP4 = ytFiles.get(itag).getUrl();
//                        } catch (Exception ex2) {
//                            itag = 133;//240kps
//                            try {
//                                urlMP4 = ytFiles.get(itag).getUrl();
//                            } catch (Exception ex3) {
//                                itag = 17;//144kps
//                                try {
//                                    urlMP4 = ytFiles.get(itag).getUrl();
//                                } catch (Exception ex4) {
//
//                                }
//                            }
//                        }
//                    }
//                }
//                Log.d("itagURL","320kps: "+itag);
//            }
//        }
//        return urlMP4;
//    }
//
//    //Get link audio
//    public String getLinkM4ADownload(SparseArray<YtFile> ytFiles)
//    {
//        String urlM4A="";
//        if (ytFiles != null) {
//            int itag = 141;
//            try {
//                urlM4A = ytFiles.get(itag).getUrl();
//            } catch (Exception ex) {
//                itag = 140;
//                try {
//                    urlM4A = ytFiles.get(itag).getUrl();
//                } catch (Exception ex1) {
//                    itag = 139;
//                    try {
//                        urlM4A = ytFiles.get(itag).getUrl();
//                    } catch (Exception ex2) {
//                    }
//                }
//            }
//            Log.d("itagURL","128kps: "+itag);
//        }
//        return  urlM4A;
//    }
}