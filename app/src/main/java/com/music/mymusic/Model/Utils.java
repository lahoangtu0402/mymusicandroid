package com.music.mymusic.Model;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;

import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.RequiresApi;


import com.music.mymusic.MainActivity;
import com.music.mymusic.MediaPlayerService;
import com.music.mymusic.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class Utils {

    private static Utils instance;
    public static Utils init(){
        if(instance == null){
            instance = new Utils();
        }
        return instance;
    }
    public int readWidth(Activity context) {
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        return width;
    }

    public void shareSongs(String title, String imageUrl, Activity activity){
        Uri imageUri = Uri.parse(imageUrl);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT,
                title + " Watch : " + "https://cotomovies.com");
        intent.putExtra(Intent.EXTRA_STREAM, imageUri);
        //intent.setType("image/jpeg");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("text/plain");
        activity.startActivity(Intent.createChooser(intent, "Share Song" ));
    }

    public float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public float pxToSp(Context context, float px) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return px/scaledDensity;
    }

    public int spToPx(float sp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }
    public float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }
    public Drawable getDrawable(String name, Context context) {
        int resourceId = context.getResources().getIdentifier(name, "drawable", context.getPackageName());
        return context.getResources().getDrawable(resourceId);
    }
    public String getTimeFromString(String duration) {
        // TODO Auto-generated method stub
        String time = "";
        boolean hourexists = false, minutesexists = false, secondsexists = false;
        if (duration.contains("H"))
            hourexists = true;
        if (duration.contains("M"))
            minutesexists = true;
        if (duration.contains("S"))
            secondsexists = true;
        if (hourexists) {
            String hour = "";
            hour = duration.substring(duration.indexOf("T") + 1,
                    duration.indexOf("H"));
            if (hour.length() == 1)
                hour = "0" + hour;
            time += hour + ":";
        }
        if (minutesexists) {
            String minutes = "";
            if (hourexists)
                minutes = duration.substring(duration.indexOf("H") + 1,
                        duration.indexOf("M"));
            else
                minutes = duration.substring(duration.indexOf("T") + 1,
                        duration.indexOf("M"));
            if (minutes.length() == 1)
                minutes = "0" + minutes;
            time += minutes + ":";
        } else {
            time += "00:";
        }
        if (secondsexists) {
            String seconds = "";
            if (hourexists) {
                if (minutesexists)
                    seconds = duration.substring(duration.indexOf("M") + 1,
                            duration.indexOf("S"));
                else
                    seconds = duration.substring(duration.indexOf("H") + 1,
                            duration.indexOf("S"));
            } else if (minutesexists){
                seconds = duration.substring(duration.indexOf("M") + 1,
                        duration.indexOf("S"));
            }
            else
                seconds = duration.substring(duration.indexOf("T") + 1,
                        duration.indexOf("S"));
            if (seconds.length() == 1)
                seconds = "0" + seconds;
            time += seconds;
        }else{
            time += "00";
        }
        return time;
    }


    public void showNotification(Context context, String title, String artist, String icon){
        MediaPlayerService mediaPlayerService = MediaPlayerService.getInstance();
        MainActivity mainActivity = MainActivity.getInstance();
        mainActivity.mediaArtist =  artist;
        mainActivity.mediaTitle = title;
        mainActivity.mediaLargeIcon = icon;
        Intent intent = new Intent( context, MediaPlayerService.class );
        intent.setAction( MediaPlayerService.ACTION_PLAY );
        try {
            context.startService(intent);
        }catch (Exception e){

        }
    }



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void buildNotification(Notification.Action action, Context context ) {
        Intent intentService = new Intent( context, MediaPlayerService.class );
        MainActivity mainActivity = MainActivity.getInstance();
        MediaPlayerService mediaPlayerService = MediaPlayerService.getInstance();
        mediaPlayerService.notificationManager = (NotificationManager) context.getSystemService( Context.NOTIFICATION_SERVICE );
        Notification.MediaStyle style = new Notification.MediaStyle();
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("DATA", "notification");
        PendingIntent pendingIntent1 = PendingIntent.getService(context, 1, intent, 0);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        CharSequence name = context.getString(R.string.channel_name);// The user-visible name of the channel.
        Notification.Builder builder = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            //setting pattern to disable vibrating
            notificationChannel.setVibrationPattern(new long[]{ 0 });
            notificationChannel.enableVibration(true);
            notificationChannel.setShowBadge(false);
            mediaPlayerService.notificationManager.createNotificationChannel(notificationChannel);
//            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(notificationChannel);
            builder = new Notification.Builder(context,CHANNEL_ID);
        }else{
            builder = new Notification.Builder(context);
        }
        builder.setSmallIcon(R.drawable.iconapp);
        builder.setContentTitle( mainActivity.mediaTitle );
        builder.setContentText(  mainActivity.mediaArtist );
        builder.setOngoing(true);
        builder.setContentIntent(pendingIntent);
        builder.setDeleteIntent( pendingIntent1 );
        builder.setStyle(style);
        builder.build().flags = Notification.FLAG_ONGOING_EVENT;
        builder.addAction( generateAction( android.R.drawable.ic_media_previous, "Previous", mediaPlayerService.ACTION_PREVIOUS,context ) );
        builder.addAction( action );
        builder.addAction( generateAction( android.R.drawable.ic_media_next, "Next", mediaPlayerService.ACTION_NEXT,context ) );
        style.setShowActionsInCompactView(0,1,2,3,4);
        Bitmap bitmap_image = BitmapFactory.decodeResource(context.getResources(), R.drawable.iconapp);
        Notification.Builder finalBuilder = builder;
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                finalBuilder.setLargeIcon(bitmap);
                    mediaPlayerService.notificationManager.notify( 1, finalBuilder.build());
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                finalBuilder.setLargeIcon(bitmap_image);
                    mediaPlayerService.notificationManager.notify( 1, finalBuilder.build());
            }
            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                finalBuilder.setLargeIcon(bitmap_image);
                    mediaPlayerService.notificationManager.notify( 1, finalBuilder.build());
            }
        };


        Picasso.get().load(mainActivity.mediaLargeIcon).resize(80, 80).into(target);
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT_WATCH)
    public Notification.Action generateAction(int icon, String title, String intentAction , Context context) {
        Intent intent = new Intent(context , MediaPlayerService.class );
        intent.setAction( intentAction );
        PendingIntent pendingIntent = PendingIntent.getService(context, 1, intent, 0);
        return new Notification.Action.Builder( icon, title, pendingIntent ).build();
    }
    public boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
