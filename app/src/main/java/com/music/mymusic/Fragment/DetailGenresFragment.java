package com.music.mymusic.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.music.mymusic.Adapter.ItemClick;
import com.music.mymusic.Adapter.ListPlaylistAdapter;
import com.music.mymusic.Adapter.VideoAdapter;
import com.music.mymusic.MainActivity;
import com.music.mymusic.Model.CPlayList;
import com.music.mymusic.Model.CVideo;
import com.music.mymusic.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class DetailGenresFragment extends Fragment {

    private static DetailGenresFragment instance;
    public static DetailGenresFragment getInstance() {
        if (instance == null) {
            synchronized (DetailGenresFragment.class) {
                instance = new DetailGenresFragment();
            }
        }
        return instance;
    }


    View view;
    RequestQueue requestQueue;
    MainActivity mainActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = MainActivity.getInstance();
        requestQueue =  Volley.newRequestQueue(getActivity());
        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_detail_genres, null, false);
        rcv_data = view.findViewById(R.id.rcv_data);
        arrayList = new ArrayList<>();
        adapter = new ListPlaylistAdapter(getActivity(),arrayList);
        adapter.setItemClick(new ItemClick() {
            @Override
            public void onItemClick(int position) {
                DetailPlaylistFragment fragment = DetailPlaylistFragment.getInstance();
                FragmentTransaction transaction = mainActivity.fragmentManager.beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_bottom,  R.anim.slide_in_top,R.anim.fade_in,R.anim.slide_out_bottom);
                transaction.replace(R.id.fragment_container, fragment);
                Bundle bundle = new Bundle();
                bundle.putString("id", arrayList.get(position).getPlayList_key()+"");
                bundle.putString("tittle", arrayList.get(position).getTen()+"");
                bundle.putString("image", arrayList.get(position).getHinh()+"");
                fragment.setArguments(bundle);

                transaction.addToBackStack(null);
                transaction.commit();
                Animation slideUp = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
                if (mainActivity.navigation.getVisibility() == View.VISIBLE) {
                    mainActivity.navigation.startAnimation(slideUp);
                    //    mainActivity.navigation.setAlpha(0);
                    mainActivity.navigation.setVisibility(View.GONE);
                }
            }
        });
        rcv_data.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rcv_data.setAdapter(adapter);
        toolbar = view.findViewById(R.id.toolBar);
        imageClose = view.findViewById(R.id.imageClose);
        image_detail = view.findViewById(R.id.image_detail);
        Bundle extras = getArguments();
        String id = extras.getString("id");
        String TITLE = extras.getString("name");
        String HINH = extras.getString("image");
        toolbar.setTitle(TITLE);
        Picasso.get().load(HINH).into(image_detail);
        loadData(id);
    }

    RecyclerView rcv_data;
    ListPlaylistAdapter adapter;
    ArrayList<CPlayList> arrayList;
    Toolbar toolbar;
    ImageView imageClose,image_detail;

    void loadData(String id ){
        String url = "http://beta.blackdog.vip/api/music/getplaylistbycategory/"+id;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i<jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        arrayList.add(new CPlayList(jsonObject.getString("thumb"),
                                jsonObject.getString("playlist_name"),
                                jsonObject.getString("playlist_key"),
                                jsonObject.getString("playlist_description")));
                    }
                    adapter.notifyDataSetChanged();
                }catch (Exception e){

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return view;

    }



    @Override
    public void onResume() {
        super.onResume();
        Animation slideUp = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
        if (mainActivity.navigation.getVisibility() == View.VISIBLE) {
            mainActivity.navigation.startAnimation(slideUp);
            mainActivity.navigation.setVisibility(View.GONE);
        }
    }
    @Override
    public void onPause() {
        super.onPause();
    }
}