package com.music.mymusic.Fragment;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.music.mymusic.Adapter.AdapterMoodGenres;
import com.music.mymusic.Adapter.CCategory;
import com.music.mymusic.Adapter.ItemClick;
import com.music.mymusic.Adapter.VideoAdapter;
import com.music.mymusic.MainActivity;
import com.music.mymusic.Model.CVideo;
import com.music.mymusic.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class DetailPlaylistFragment extends Fragment {

    private static DetailPlaylistFragment instance;
    public static DetailPlaylistFragment getInstance() {
        if (instance == null) {
            synchronized (DetailPlaylistFragment.class) {
                instance = new DetailPlaylistFragment();
            }
        }
        return instance;
    }

    View view;
    MainActivity mainActivity;
    RequestQueue requestQueue;
    ImageView image_detailPlaylist,imageClose;
    Toolbar toolbarDetailPlaylist;
    RecyclerView song_list;
    VideoAdapter videoAdapter;
    ArrayList<CVideo> arrayList;
    FloatingActionButton fab;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = MainActivity.getInstance();
        requestQueue =  Volley.newRequestQueue(getActivity());
        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_detail_playlist, null, false);
        Bundle extras = getArguments();
        String KEY = extras.getString("id");
        String TITLE = extras.getString("tittle");
        String HINH = extras.getString("image");

        image_detailPlaylist = view.findViewById(R.id.image_detailPlaylist);
        Picasso.get().load(HINH).into(image_detailPlaylist);

        imageClose = view.findViewById(R.id.imageClose);
        toolbarDetailPlaylist = view.findViewById(R.id.toolbarDetailPlaylist);
        song_list = view.findViewById(R.id.song_list);
        toolbarDetailPlaylist.setTitle(TITLE);

        song_list = view.findViewById(R.id.song_list);
        arrayList = new ArrayList<>();
        videoAdapter = new VideoAdapter(getActivity(),arrayList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        song_list.setLayoutManager(linearLayoutManager);
        song_list.setAdapter(videoAdapter);
        videoAdapter.setItemClick(new ItemClick() {
            @Override
            public void onItemClick(int position) {
                mainActivity.playWithUrl(arrayList.get(position),arrayList,TITLE,position);
                mainActivity.slideUp.show();
            }
        });
        loadData(KEY);
        imageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (arrayList.size() != 0){
                    mainActivity.playWithUrl(arrayList.get(0),arrayList,TITLE,0);
                    mainActivity.slideUp.show();
                }

            }
        });

    }
    private void loadData(String KEYPLAYLIST) {
        String url = "https://www.googleapis.com/youtube/v3/playlistItems?maxResults=50&fields=items(snippet(title,description,thumbnails(high(url)),resourceId(videoId)))&part=snippet&playlistId="+KEYPLAYLIST+"&key=AIzaSyDj3ofX4rAx76SuGdYrLxG_uE9NvMLU_pI";
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        String title = "";
                        String description = "";
                        String idVideo = "";
                        String url = "";

                        try {
                            JSONArray jsonArray = response.getJSONArray("items");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                JSONObject jsonSnippet = jsonObject.getJSONObject("snippet");
                                title = jsonSnippet.getString("title");
                                JSONObject jsonThumbnail = jsonSnippet.getJSONObject("thumbnails");
                                JSONObject jsonHigh = jsonThumbnail.getJSONObject("high");
                                url = jsonHigh.getString("url");
                                JSONObject jsonResource = jsonSnippet.getJSONObject("resourceId");
                                idVideo = jsonResource.getString("videoId");

                                arrayList.add(new CVideo(idVideo,title,"",url));
                            }
                            videoAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return view;

    }



    @Override
    public void onResume() {
        super.onResume();
        Animation slideUp = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
        if (mainActivity.navigation.getVisibility() == View.VISIBLE) {
            mainActivity.navigation.startAnimation(slideUp);
            mainActivity.navigation.setVisibility(View.GONE);
        }
    }

}