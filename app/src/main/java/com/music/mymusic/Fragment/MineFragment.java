package com.music.mymusic.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.youtube.YouTubeScopes;
import com.music.mymusic.Adapter.AdapterMoodGenres;
import com.music.mymusic.Adapter.CCategory;
import com.music.mymusic.Adapter.ItemClick;
import com.music.mymusic.Adapter.VideoAdapter;
import com.music.mymusic.Adapter.VideoDownloadAdapter;
import com.music.mymusic.Adapter.VideoMineAdapter;
import com.music.mymusic.Database.CVideoHelper;
import com.music.mymusic.Database.DownloadHelper;
import com.music.mymusic.MainActivity;
import com.music.mymusic.Model.CVideo;
import com.music.mymusic.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;


public class MineFragment extends Fragment {

    private static MineFragment instance;
    public static MineFragment getInstance() {
        if (instance == null) {
            synchronized (MineFragment.class) {
                instance = new MineFragment();
            }
        }
        return instance;
    }


    View view;
    RequestQueue requestQueue;
    MainActivity mainActivity;

    RecyclerView rcv_fav,rcv_watched,rcv_download;
    VideoMineAdapter favAdapter,watchAdapter;
    ArrayList<CVideo> favArraylist, watchArraylist,downArraylist;

    VideoDownloadAdapter downloadAdapter;
    CVideoHelper cVideoHelper;
    DownloadHelper downloadHelper;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = MainActivity.getInstance();
        requestQueue =  Volley.newRequestQueue(getActivity());
        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_mine, null, false);
        cVideoHelper = new CVideoHelper(getActivity());
        downloadHelper = new DownloadHelper(getActivity());

        rcv_fav = view.findViewById(R.id.rcv_fav);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        favArraylist = new ArrayList<>();
        favAdapter = new VideoMineAdapter(getActivity(),favArraylist);
        favAdapter.setItemClick(new ItemClick() {
            @Override
            public void onItemClick(int position) {
                ArrayList<CVideo> arrayList = new ArrayList<>();
                arrayList.add(favArraylist.get(position));
                mainActivity.playWithUrl(favArraylist.get(position),arrayList,"Favorites",0);
                mainActivity.slideUp.show();
            }
        });
        rcv_fav.setAdapter(favAdapter);
        rcv_fav.setLayoutManager(linearLayoutManager);

        //
        rcv_watched = view.findViewById(R.id.rcv_watched);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getActivity());
        linearLayoutManager2.setOrientation(RecyclerView.HORIZONTAL);
        watchArraylist = new ArrayList<>();
        watchAdapter = new VideoMineAdapter(getActivity(),watchArraylist);
        watchAdapter.setItemClick(new ItemClick() {
            @Override
            public void onItemClick(int position) {
                ArrayList<CVideo> arrayList = new ArrayList<>();
                arrayList.add(watchArraylist.get(position));
                mainActivity.playWithUrl(watchArraylist.get(position),arrayList,"Watched",0);
                mainActivity.slideUp.show();
            }
        });
        rcv_watched.setAdapter(watchAdapter);
        rcv_watched.setLayoutManager(linearLayoutManager2);
        //

        rcv_download = view.findViewById(R.id.rcv_download);
        LinearLayoutManager linearLayoutManager3 = new LinearLayoutManager(getActivity());
        linearLayoutManager3.setOrientation(RecyclerView.HORIZONTAL);
        downArraylist = new ArrayList<>();
        downloadAdapter = new VideoDownloadAdapter(getActivity(),downArraylist);
        downloadAdapter.setItemClick(new ItemClick() {
            @Override
            public void onItemClick(int position) {
                mainActivity.playWithUrl(downArraylist.get(position),downArraylist,"Downloaded",position);
                mainActivity.slideUp.show();
            }
        });
        rcv_download.setAdapter(downloadAdapter);
        rcv_download.setLayoutManager(linearLayoutManager3);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return view;

    }

    public void loadFav(){
        if (watchArraylist != null && favAdapter!=null){
            watchArraylist.clear();
            for (CVideo cVideo : cVideoHelper.getListByGroup(0)){
                favArraylist.add(cVideo);
            }
            favAdapter.notifyDataSetChanged();
        }

    }

    void loadWatched(){
        if (watchArraylist != null && watchAdapter != null ){
            watchArraylist.clear();
            for (CVideo cVideo : cVideoHelper.getListByGroup(1)){
                watchArraylist.add(cVideo);
            }
            watchAdapter.notifyDataSetChanged();
        }

    }
    public void loadDownload(){
        if (downArraylist != null && downloadAdapter != null){
            downArraylist.clear();

            for (CVideo cVideo : downloadHelper.getList()){
                downArraylist.add(cVideo);
            }

            downloadAdapter.notifyDataSetChanged();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        loadFav();
        loadWatched();
        loadDownload();
    }
}