package com.music.mymusic.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.music.mymusic.Adapter.HomeTopAdapter;
import com.music.mymusic.Adapter.ItemClick;
import com.music.mymusic.Adapter.NewMusicAdapter;
import com.music.mymusic.MainActivity;
import com.music.mymusic.Model.CPlayList;
import com.music.mymusic.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class HomeFragment extends Fragment {

    private static HomeFragment instance;
    public static HomeFragment getInstance() {
        if (instance == null) {
            synchronized (HomeFragment.class) {
                instance = new HomeFragment();
            }
        }
        return instance;
    }

    String urlGetTopPlaylist = "http://beta.blackdog.vip/api/music/gettopplaylist";
    String urlBody = "https://blackdog.vip/data/getHomePlaylist";
//    String urlGetJsonRelease = "https://blackdog.vip/data/getHomePlaylist";
//    String urlGetJsonTodayHit = "https://blackdog.vip/data/getHomePlaylist";
    String urlGetNewMusic = "https://blackdog.vip/data/getNewMusicVideo";
//
//
    RecyclerView rcv_Top;
    ArrayList<CPlayList> arrayListTop;
    HomeTopAdapter adapterTop;
//
    RecyclerView rcv_newMusic;
    ArrayList<CPlayList> cNewMusicArrayList;
    NewMusicAdapter adapterNewMusic;
//
    RecyclerView rcv_mixed;
    ArrayList<CPlayList> cMixedArrayList;
    NewMusicAdapter adapterMixed;

    RecyclerView rcv_release;
    ArrayList<CPlayList> cReleaseArrayList;
    NewMusicAdapter adapterRelease;

    RecyclerView rcv_hitDay;
    ArrayList<CPlayList> cHitDayArrayList;
    NewMusicAdapter adapterHit;


    ImageView imageHome;
    View view;
    RequestQueue requestQueue;

    TextView txtHome;

    MainActivity mainActivity;
    CPlayList playlistTop;
    ImageView image_play;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestQueue = Volley.newRequestQueue(getActivity());
        mainActivity = MainActivity.getInstance();

        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_home, null, false);
        imageHome = view.findViewById(R.id.imageHome);
        txtHome = view.findViewById(R.id.txtHome);
        rcv_Top = view.findViewById(R.id.rcv_Top);
        rcv_newMusic = view.findViewById(R.id.rcv_newMusic);
        rcv_mixed = view.findViewById(R.id.rcv_mixed);
        rcv_release = view.findViewById(R.id.rcv_newRelease);
        rcv_hitDay = view.findViewById(R.id.rcv_hitDay);
//
        arrayListTop = new ArrayList<CPlayList>();
        adapterTop = new HomeTopAdapter(getActivity(), arrayListTop);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        rcv_Top.setAdapter(adapterTop);
        rcv_Top.setLayoutManager(linearLayoutManager);
        adapterTop.setItemClick(new ItemClick() {
            @Override
            public void onItemClick(int position) {
                startDetailPlaylist(arrayListTop.get(position));

            }
        });
//
        newMusic();
        mixed();
        release();
        hitDay();
//

//        GetJsonMixed();
//        GetJsonRelease(urlGetJsonRelease);
//        GetJsonTodayHit(urlGetJsonTodayHit);
        GetJsonNewMusic(urlGetNewMusic);
        GetJsonTop(urlGetTopPlaylist);
        GetDatabody(urlBody);

        image_play = view.findViewById(R.id.image_play);
        image_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playlistTop != null) {
                    startDetailPlaylist(playlistTop);
                }
            }
        });

    }
    void startDetailPlaylist(CPlayList cPlayList){
        DetailPlaylistFragment fragment = DetailPlaylistFragment.getInstance();
        FragmentTransaction transaction = mainActivity.fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_bottom,  R.anim.slide_in_top,R.anim.fade_in,R.anim.slide_out_bottom);
        transaction.replace(R.id.fragment_container, fragment);
        Bundle bundle = new Bundle();
        bundle.putString("id", cPlayList.getPlayList_key()+"");
        bundle.putString("tittle", cPlayList.getTen()+"");
        bundle.putString("image", cPlayList.getHinh()+"");
        fragment.setArguments(bundle);

        transaction.addToBackStack(null);
        transaction.commit();
        Animation slideUp = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
        if (mainActivity.navigation.getVisibility() == View.VISIBLE) {
            mainActivity.navigation.startAnimation(slideUp);
            //    mainActivity.navigation.setAlpha(0);
            mainActivity.navigation.setVisibility(View.GONE);
        }
    }
    void newMusic() {
        cNewMusicArrayList = new ArrayList<>();
        adapterNewMusic = new NewMusicAdapter(getActivity(), cNewMusicArrayList,true);
        rcv_newMusic.setAdapter(adapterNewMusic);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getActivity());
        linearLayoutManager2.setOrientation(RecyclerView.HORIZONTAL);
        rcv_newMusic.setLayoutManager(linearLayoutManager2);
        adapterNewMusic.setItemClick(new ItemClick() {
            @Override
            public void onItemClick(int position) {
//                Intent itentNewMusic = new Intent(getActivity(), PlayListHome.class);
//                itentNewMusic.putExtra("playlist_key",cNewMusicArrayList.get(position).getPlayList_key());
//                getActivity().startActivity(itentNewMusic);
                startDetailPlaylist(cNewMusicArrayList.get(position));
            }
        });
    }

    void mixed() {

        cMixedArrayList = new ArrayList<>();
        adapterMixed = new NewMusicAdapter(getActivity(), cMixedArrayList,false);

        LinearLayoutManager linearLayoutManager3 = new LinearLayoutManager(getActivity());
        linearLayoutManager3.setOrientation(RecyclerView.HORIZONTAL);

        rcv_mixed.setAdapter(adapterMixed);
        rcv_mixed.setLayoutManager(linearLayoutManager3);

        adapterMixed.setItemClick(new ItemClick() {
            @Override
            public void onItemClick(int position) {
                startDetailPlaylist(cMixedArrayList.get(position));

            }
        });
    }


    void release() {

        cReleaseArrayList = new ArrayList<>();
        adapterRelease = new NewMusicAdapter(getActivity(), cReleaseArrayList,false);

        rcv_release.setAdapter(adapterRelease);

        LinearLayoutManager linearLayoutManager4 = new LinearLayoutManager(getActivity());
        linearLayoutManager4.setOrientation(RecyclerView.HORIZONTAL);

        rcv_release.setLayoutManager(linearLayoutManager4);

        adapterRelease.setItemClick(new ItemClick() {
            @Override
            public void onItemClick(int position) {
                startDetailPlaylist(cReleaseArrayList.get(position));

            }
        });
    }

    void hitDay() {

        cHitDayArrayList = new ArrayList<>();
        adapterHit = new NewMusicAdapter(getActivity(), cHitDayArrayList,false);

        LinearLayoutManager linearLayoutManager5 = new LinearLayoutManager(getActivity());
        linearLayoutManager5.setOrientation(RecyclerView.HORIZONTAL);

        rcv_hitDay.setAdapter(adapterHit);
        rcv_hitDay.setLayoutManager(linearLayoutManager5);

        adapterHit.setItemClick(new ItemClick() {
            @Override
            public void onItemClick(int position) {
                startDetailPlaylist(cHitDayArrayList.get(position));
            }
        });
    }


    private void GetJsonTop(String url) {

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        String playlist_key = "";
                        String playlist_name = "";
                        String playlist_description = "";
                        String thumb = "";

                        for (int i = 1; i < response.length(); i++) {

                            try {
                                JSONObject jsonObject = response.getJSONObject(i);
                                playlist_key = jsonObject.getString("playlist_key");
                                playlist_name = jsonObject.getString("playlist_name");
                                playlist_description = jsonObject.getString("playlist_description");
                                thumb = jsonObject.getString("thumb");


                                arrayListTop.add(new CPlayList(thumb,playlist_name,playlist_key,playlist_description));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        try {
                            JSONObject jsonObject = response.getJSONObject(0);
                            Picasso.get().load(jsonObject.getString("thumb")).into(imageHome);
                            txtHome.setText(jsonObject.getString("playlist_name"));
                            playlistTop = new CPlayList(jsonObject.getString("thumb"),
                                    jsonObject.getString("playlist_name"),
                                    jsonObject.getString("playlist_key"),
                                    jsonObject.getString("playlist_description"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                        adapterTop.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Toast.makeText(getActivity(), "Error!!", Toast.LENGTH_SHORT).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
    private void GetJsonNewMusic(String url) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        String playlist_key = "";
                        String playlist_name = "";
                        String playlist_description = "";
                        String thumb = "";

                        try {
                            JSONArray jsonArray = response.getJSONArray("listplaylist");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                playlist_key = jsonObject.getString("playlist_key");
                                playlist_name = jsonObject.getString("playlist_name");
                                thumb = jsonObject.getString("thumb");
                                playlist_description = jsonObject.getString("playlist_description");

                                cNewMusicArrayList.add(new CPlayList(thumb, playlist_name, playlist_key,playlist_description ));
                            }
                            adapterNewMusic.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Error!!!", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    private void GetDatabody(String url) {
        JsonArrayRequest jsonArrayRequest2 = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            String playlist_key = "";
                            String playlist_name = "";
                            String thumb = "";
                            String playlist_description = "";

                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                JSONArray jsonListPlayList = jsonObject.getJSONArray("listplaylist");
                                for (int k = 0; k < jsonListPlayList.length(); k++) {
                                    JSONObject jsonObject1 = jsonListPlayList.getJSONObject(k);
                                    playlist_key = jsonObject1.getString("playlist_key");
                                    playlist_name = jsonObject1.getString("playlist_name");
                                    thumb = jsonObject1.getString("thumb");
                                    playlist_description = jsonObject1.getString("playlist_description");
                                    switch (i){
                                        case 0:
                                            cMixedArrayList.add(new CPlayList(thumb, playlist_name, playlist_key,playlist_description ));
                                            break;
                                        case 1:
                                            cReleaseArrayList.add(new CPlayList(thumb, playlist_name, playlist_key,playlist_description ));
                                            break;
                                        case 2:
                                            cHitDayArrayList.add(new CPlayList(thumb, playlist_name, playlist_key,playlist_description ));
                                            break;
                                    }
                                }

                                adapterMixed.notifyDataSetChanged();
                                adapterHit.notifyDataSetChanged();
                                adapterRelease.notifyDataSetChanged();
                            }
                        }catch (Exception e){

                        }

                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getActivity(), "Error!!", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonArrayRequest2);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mainActivity.navigation.getVisibility() == View.GONE) {
            Animation slideUp = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
            mainActivity.navigation.startAnimation(slideUp);
            mainActivity.navigation.setAlpha(1);
            mainActivity.navigation.setVisibility(View.VISIBLE);
        }
    }
}