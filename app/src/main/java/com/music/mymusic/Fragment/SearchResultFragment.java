package com.music.mymusic.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.music.mymusic.Adapter.ItemClick;
import com.music.mymusic.Adapter.VideoAdapter;
import com.music.mymusic.MainActivity;
import com.music.mymusic.Model.CVideo;
import com.music.mymusic.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchResultFragment extends Fragment {

    private static SearchResultFragment instance;
    public static SearchResultFragment getInstance() {
        if (instance == null) {
            synchronized (SearchResultFragment.class) {
                instance = new SearchResultFragment();
            }
        }
        return instance;
    }


    View view;
    RequestQueue requestQueue;
    MainActivity mainActivity;

    FloatingSearchView searchView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = MainActivity.getInstance();
        requestQueue =  Volley.newRequestQueue(getActivity());
        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_search_result, null, false);
        searchView = view.findViewById(R.id.searchView);
        searchView.setOnHomeActionClickListener(new FloatingSearchView.OnHomeActionClickListener() {
            @Override
            public void onHomeClicked() {
                getFragmentManager().popBackStack();
            }
        });
        Bundle extras = getArguments();
        queryCurrent = extras.getString("query");
        searchView.setSearchText(queryCurrent);

        arrayList = new ArrayList<>();
        rcv_data = view.findViewById(R.id.rcv_data);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rcv_data.setLayoutManager(linearLayoutManager);
        videoAdapter = new VideoAdapter(getActivity(),arrayList,false);
        videoAdapter.setItemClick(new ItemClick() {
            @Override
            public void onItemClick(int position) {
                ArrayList<CVideo> arrayListTemp = new ArrayList<>();
                arrayListTemp.add(arrayList.get(position));
                mainActivity.playWithUrl(arrayList.get(position),arrayListTemp," ",0);
                mainActivity.slideUp.show();
            }
        });
        rcv_data.setAdapter(videoAdapter);
        loadData(queryCurrent);

        searchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {

            }

            @Override
            public void onSearchAction(String currentQuery) {
                queryCurrent = currentQuery;
                loadData(currentQuery);
            }
        });
    }
    String queryCurrent = "";
    ArrayList<CVideo> arrayList;
    VideoAdapter videoAdapter;
    RecyclerView rcv_data;

    private void loadData(String queryCurrent) {
        arrayList.clear();
        try {
             queryCurrent = URLEncoder.encode(queryCurrent, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = "https://www.googleapis.com/youtube/v3/search?part=snippet&q="+queryCurrent+"&type=video&maxResults=30&pageToken=&key="+MainActivity.API_key;
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("items");
                            for (int i = 0; i<jsonArray.length(); i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                JSONObject objectID = jsonObject.getJSONObject("id");
                                String id = objectID.getString("videoId");

                                JSONObject snippet = jsonObject.getJSONObject("snippet");
                                String title = snippet.getString("title");
                                String description = snippet.getString("description");
                                JSONObject thumbnails = snippet.getJSONObject("thumbnails");
                                JSONObject medium = thumbnails.getJSONObject("medium");
                                String thumb = medium.getString("url");

                                arrayList.add(new CVideo(id,title,description,thumb));
                            }
                            videoAdapter.notifyDataSetChanged();

                        }catch (Exception e){
                            Log.d("query",e.getMessage()+"");
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("query",error.getMessage()+"");
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return view;

    }



    @Override
    public void onResume() {
        super.onResume();

    }
    @Override
    public void onPause() {
        super.onPause();
        Animation slideUp = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
        if (mainActivity.navigation.getVisibility() == View.GONE) {
            mainActivity.navigation.startAnimation(slideUp);
            mainActivity.navigation.setAlpha(1);
            mainActivity.navigation.setVisibility(View.VISIBLE);
        }

    }
}