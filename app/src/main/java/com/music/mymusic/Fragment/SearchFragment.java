package com.music.mymusic.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.music.mymusic.Adapter.AdapterMoodGenres;
import com.music.mymusic.Adapter.CCategory;
import com.music.mymusic.Adapter.ItemClick;
import com.music.mymusic.Adapter.VideoAdapter;
import com.music.mymusic.MainActivity;
import com.music.mymusic.Model.CVideo;
import com.music.mymusic.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class SearchFragment extends Fragment {

    private static SearchFragment instance;
    public static SearchFragment getInstance() {
        if (instance == null) {
            synchronized (SearchFragment.class) {
                instance = new SearchFragment();
            }
        }
        return instance;
    }

    RecyclerView rcv_moodGenres;
    ArrayList<CCategory> cMoodGenresArrayList;
    AdapterMoodGenres adapterMoodGenres;

    String urlGetJsonMoodGenres = "https://beta.blackdog.vip/api/music/getlistcategory";
    String urlGetJsonTrending = "https://www.googleapis.com/youtube/v3/videos?maxResults=20&fields=items(id,snippet(title,description,thumbnails(medium(url))))&part=snippet&chart=mostPopular&key=AIzaSyDj3ofX4rAx76SuGdYrLxG_uE9NvMLU_pI";
    String urlGetJsonTrendingVN = "https://www.googleapis.com/youtube/v3/videos?maxResults=50&fields=items(id,snippet(title,description,thumbnails))&part=snippet&chart=mostPopular&regionCode=vn&key="+MainActivity.API_key;
    View view;

    RecyclerView rcv_trendingVN;
    ArrayList<CVideo> cTrendingVNArrayList;
    VideoAdapter adapterTrendingVN;

    RecyclerView rcv_trendingVideo;
    ArrayList<CVideo> cTrendingVideoArrayList;
    VideoAdapter adapterTrendingVideo;
    RequestQueue requestQueue;
    MainActivity mainActivity;
    FloatingSearchView floatingSearchView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = MainActivity.getInstance();
        requestQueue =  Volley.newRequestQueue(getActivity());
        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_search, null, false);

        rcv_moodGenres = view.findViewById(R.id.rcv_moodGenres);
        moodGenres();

        rcv_trendingVN = view.findViewById(R.id.rcv_trendingVN);
        rcv_trendingVideo = view.findViewById(R.id.rcv_trendingVideo);

        trendingVideoVN();
        trendingVideo();

        GetJsonMoodGenres(urlGetJsonMoodGenres);
        GetJsonTrending(urlGetJsonTrending);
        GetJsonTrendingVN(urlGetJsonTrendingVN);
        floatingSearchView = view.findViewById(R.id.searchView);
        floatingSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {

            }

            @Override
            public void onSearchAction(String currentQuery) {
                SearchResultFragment fragment = SearchResultFragment.getInstance();
                FragmentTransaction transaction = mainActivity.fragmentManager.beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_bottom,  R.anim.slide_in_top,R.anim.fade_in,R.anim.slide_out_bottom);
                transaction.replace(R.id.fragment_container, fragment);
                Bundle bundle = new Bundle();
                bundle.putString("query", currentQuery);

                fragment.setArguments(bundle);

                transaction.addToBackStack(null);
                transaction.commit();
                Animation slideUp = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
                if (mainActivity.navigation.getVisibility() == View.VISIBLE) {
                    mainActivity.navigation.startAnimation(slideUp);
                    //    mainActivity.navigation.setAlpha(0);
                    mainActivity.navigation.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return view;

    }

    private void GetJsonMoodGenres(String url) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                String name = "";
                int id = 0;
                String image = "";

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);

                        name = jsonObject.getString("name");
                        id = jsonObject.getInt("id");
                        try {
                            image = jsonObject.getString("image");
                        }catch (Exception e){
                            image = "";
                        }
                        cMoodGenresArrayList.add(new CCategory(id,name,image));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapterMoodGenres.notifyDataSetChanged();

                    }
                });


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Error!!!", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }

    void moodGenres() {
        cMoodGenresArrayList = new ArrayList<>();
        adapterMoodGenres = new AdapterMoodGenres(getActivity(), cMoodGenresArrayList);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.HORIZONTAL, false);
        rcv_moodGenres.setLayoutManager(manager);
        rcv_moodGenres.setAdapter(adapterMoodGenres);
        adapterMoodGenres.setItemClick(new ItemClick() {
            @Override
            public void onItemClick(int position) {
                DetailGenresFragment fragment = DetailGenresFragment.getInstance();
                FragmentTransaction transaction = mainActivity.fragmentManager.beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_bottom,  R.anim.slide_in_top,R.anim.fade_in,R.anim.slide_out_bottom);
                transaction.replace(R.id.fragment_container, fragment);
                Bundle bundle = new Bundle();
                bundle.putString("id", String.valueOf(cMoodGenresArrayList.get(position).getId()));
                bundle.putString("name", String.valueOf(cMoodGenresArrayList.get(position).getName()));
                bundle.putString("image", String.valueOf(cMoodGenresArrayList.get(position).getImage()));

                fragment.setArguments(bundle);

                transaction.addToBackStack(null);
                transaction.commit();
                Animation slideUp = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
                if (mainActivity.navigation.getVisibility() == View.VISIBLE) {
                    mainActivity.navigation.startAnimation(slideUp);
                    //    mainActivity.navigation.setAlpha(0);
                    mainActivity.navigation.setVisibility(View.GONE);
                }
            }
        });
    }


    private void GetJsonTrendingVN(String url) {
        JsonObjectRequest jsonObjectRequest1 = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                String idVideo = "";
                String title = "";
                String description = "";
                String url = "";

                try {
                    JSONArray jsonArrayItems = response.getJSONArray("items");
                    int length = jsonArrayItems.length();

                    for (int i = 0; i < length; i++) {

                        JSONObject jsonObjectItem = jsonArrayItems.getJSONObject(i);

                        idVideo = jsonObjectItem.getString("id");

                        JSONObject jsonSnippet = jsonObjectItem.getJSONObject("snippet");

                        title = jsonSnippet.getString("title");
                        description = jsonSnippet.getString("description");

                        JSONObject jsonThumbnail = jsonSnippet.getJSONObject("thumbnails");
                        JSONObject jsonMedium = jsonThumbnail.getJSONObject("medium");

                        url = jsonMedium.getString("url");

                        cTrendingVNArrayList.add(new CVideo(idVideo,title,description,url));

                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterTrendingVN.notifyDataSetChanged();
                        }
                    });


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Error!!!", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonObjectRequest1);


    }

    private void GetJsonTrending(String url) {
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        String idVideo = "";
                        String title = "";
                        String description = "";
                        String url = "";

                        try {
                            JSONArray jsonArrayItems = response.getJSONArray("items");
                            for (int i = 0; i < jsonArrayItems.length(); i++) {

                                JSONObject jsonObjectItem = jsonArrayItems.getJSONObject(i);

                                idVideo = jsonObjectItem.getString("id");

                                JSONObject jsonSnippet = jsonObjectItem.getJSONObject("snippet");

                                title = jsonSnippet.getString("title");
                                description = jsonSnippet.getString("description");

                                JSONObject jsonThumbnail = jsonSnippet.getJSONObject("thumbnails");
                                JSONObject jsonMedium = jsonThumbnail.getJSONObject("medium");

                                url = jsonMedium.getString("url");

                                cTrendingVideoArrayList.add(new CVideo(idVideo,title,description,url));


                            }

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapterTrendingVideo.notifyDataSetChanged();
                                }
                            });


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Error!!!", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }


    void trendingVideoVN() {
        cTrendingVNArrayList = new ArrayList<>();
        adapterTrendingVN = new VideoAdapter(getActivity(), cTrendingVNArrayList, true);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 4, GridLayoutManager.HORIZONTAL, false);
        rcv_trendingVN.setLayoutManager(manager);

        rcv_trendingVN.setAdapter(adapterTrendingVN);
        adapterTrendingVN.setItemClick(new ItemClick() {
            @Override
            public void onItemClick(int position) {
                mainActivity.slideUp.show();
                mainActivity.playWithUrl(cTrendingVNArrayList.get(position),cTrendingVNArrayList,"Trending In VietNam",position);
            }
        });
    }
    void trendingVideo() {

        cTrendingVideoArrayList = new ArrayList<>();

        adapterTrendingVideo = new VideoAdapter(getActivity(), cTrendingVideoArrayList,true);

        GridLayoutManager manager = new GridLayoutManager(getActivity(), 4, GridLayoutManager.HORIZONTAL, false);
        rcv_trendingVideo.setLayoutManager(manager);

        rcv_trendingVideo.setAdapter(adapterTrendingVideo);

        adapterTrendingVideo.setItemClick(new ItemClick() {
            @Override
            public void onItemClick(int position) {
                mainActivity.slideUp.show();
                mainActivity.playWithUrl(cTrendingVNArrayList.get(position),cTrendingVNArrayList,"Trending In Word",position);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mainActivity.navigation.getVisibility() == View.GONE) {
            Animation slideUp = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
            mainActivity.navigation.startAnimation(slideUp);
            mainActivity.navigation.setAlpha(1);
            mainActivity.navigation.setVisibility(View.VISIBLE);
        }

    }
}

