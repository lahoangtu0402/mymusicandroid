package com.music.mymusic.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.music.mymusic.Adapter.ItemClick;
import com.music.mymusic.Adapter.VideoDownloadAdapter;
import com.music.mymusic.Adapter.VideoMineAdapter;
import com.music.mymusic.Adapter.VideoPlaylistAdapter;
import com.music.mymusic.Database.CVideoHelper;
import com.music.mymusic.Database.DownloadHelper;
import com.music.mymusic.MainActivity;
import com.music.mymusic.Model.CVideo;
import com.music.mymusic.R;

import java.util.ArrayList;


public class PlaylistFragment extends Fragment {

    private static PlaylistFragment instance;

    public static PlaylistFragment getInstance() {
        if (instance == null) {
            synchronized (PlaylistFragment.class) {
                instance = new PlaylistFragment();
            }
        }
        return instance;
    }


    View view;
    RequestQueue requestQueue;
    MainActivity mainActivity;

    RecyclerView rcv_data;
    VideoPlaylistAdapter adapter;
    ArrayList<CVideo> arrayList;
    ImageView btnClose;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = MainActivity.getInstance();
        requestQueue = Volley.newRequestQueue(getActivity());
        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_playlist, null, false);

        rcv_data = view.findViewById(R.id.rcv_data);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        arrayList = new ArrayList<>();
        adapter = new VideoPlaylistAdapter(getActivity(),arrayList);
        rcv_data.setAdapter(adapter);
        rcv_data.setLayoutManager(linearLayoutManager);
        Bundle extras = getArguments();
        String data = extras.getString("data");
        int position = extras.getInt("position");
        Log.d("data",data);
        CVideo[] flowers = new Gson().fromJson(data,CVideo[].class);
        for (int i = 0 ; i< flowers.length ; i++){
            arrayList.add(flowers[i]);
        }
        adapter.indexSelected = position;
        adapter.notifyDataSetChanged();
        adapter.setItemClick(new ItemClick() {
            @Override
            public void onItemClick(int position) {
                mainActivity.playWithUrl(arrayList.get(position),arrayList,"",position);
                getFragmentManager().popBackStack();
            }
        });
        btnClose = view.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return view;

    }

    @Override
    public void onPause() {
        super.onPause();
        mainActivity.fragment_playlist.setVisibility(View.GONE);
    }
}